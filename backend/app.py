import tensorflow as tf
import pickle
import numpy as np
import uvicorn
from fastapi import FastAPI
from schemas.model import Hoax, Output
from fastapi.middleware.cors import CORSMiddleware
from tensorflow.keras.preprocessing.sequence import pad_sequences

model_judul_path = './dl_models/bilstm_model_titleH5.h5'
model_narasi_path = './dl_models/bilstm_modelH5.h5'
new_model_judul = tf.keras.models.load_model(model_judul_path)
new_model_narasi = tf.keras.models.load_model(model_narasi_path)
tokenizer_judul_path = './tokenizer/tokenizerTitle.pickle'
tokenizer_narasi_path = './tokenizer/tokenizer2.pickle'

# unpickling tokenizer
with open(tokenizer_judul_path, "rb") as handle:
    tokenizerTitle = pickle.load(handle)

# narasi tokenizer
with open(tokenizer_narasi_path, "rb") as handle:
    tokenizerNarasi = pickle.load(handle)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:3000",
    "http://clarifact.ai"
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.get('/')
async def read_root():
    return {"message": "Hoax predictor API"}

@app.post('/predict_title', response_model=Output)
async def predict_title(data: Hoax):
    judul = data.judul
         
    judul_to_predict = tokenizerTitle.texts_to_sequences([judul])

    judul_padded = pad_sequences(judul_to_predict, padding='post', truncating='post', maxlen=1000)

    prediction = np.where(new_model_judul.predict(judul_padded) > 0.5, 1, 0)
    percentage = str(new_model_judul.predict(judul_padded)[0][0]*100)
    # classifying hoax or no hoax
    if prediction == 1:
        prediction = "Hoax"
    else:
        prediction = "Fakta"
    return {
        "prediction": prediction,
        "percentage": percentage
    }

@app.post('/predict_narasi', response_model=Output)
async def predict_narasi(data: Hoax):
    narasi = data.narasi

    narasi_to_predict = tokenizerNarasi.texts_to_sequences([narasi])

    narasi_padded = pad_sequences(narasi_to_predict, padding='post', truncating='post', maxlen=1000)

    prediction = np.where(new_model_narasi.predict(narasi_padded) > 0.5, 1, 0)
    percentage = str(new_model_narasi.predict(narasi_padded)[0][0]*100)

    # classifying hoax or no hoax
    if prediction == 1:
        prediction = "Hoax"
    else:
        prediction = "Fakta"
    return {
        "prediction": prediction,
        "percentage": percentage
    }

if __name__ == "__main__":
    uvicorn.run(app, host="localhost", port=8000)
